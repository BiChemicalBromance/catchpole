## Catchpole - A Technical Debt Collector ##

This is a small utility designed to scan through a project's source files and collect instances of intentionally introduced
technical debt/bad design decisions/cut corners etc. so that they can be dealt with at a later date, without fading into 
the general sea of TODOs. 

Is the existence of this tool promoting bad design? Maybe. Is fixing code where the bad design decisions aren't evident 
almost impossible? Definitely.
