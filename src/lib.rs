extern crate git2;
extern crate lazy_static;
extern crate regex;

use std::fmt::format;

use clang;
use lazy_static::lazy_static;
use regex::Regex;
use thiserror::Error;

#[derive(Debug)]
pub struct Config{
    path: String,
    db_path: String,
}

impl Config {
    pub fn new(path: &str, db_path:Option<&str>) -> Config{
        match db_path{
            Some(_) => {
                return Config {
                    path:String::from(path), db_path:String::from(db_path.unwrap())
                }
            }, 
            None => {
                return Config {
                    path:String::from(path), db_path:format!("{}/catchpole.db", path)
                }
            }
        }
        
    }
}



#[derive(Debug)]
pub struct ParsedComment {
    text: String,
    filename: String,
    line_number: u32,
}

impl ParsedComment {
    pub fn new(text: String, filename: String, line_number: u32) -> ParsedComment {
        ParsedComment {
            text,
            filename,
            line_number,
        }
    }

    pub fn update_text(&mut self, new_text: String) {
        self.text = new_text;
    }
}

#[derive(Error, Debug)]
pub enum CatchpoleError {
    #[error("Something about the git repo was bad")]
    BadRepo(#[from] git2::Error),
    #[error("Repo was empty")]
    EmptyRepo,
    #[error("Something about the source file was bad: {}", .0)]
    SourceFileParse(#[from] clang::SourceError),
    // This next one super duper shouldn't be an actual error condition - Option<T>
    #[error("No comments found in the source files.")]
    NoCommentsFound,
    #[error("No source range found in the parsed source file, therefore cannot tokenise")]
    NoSourceRange,
}

pub fn run(root_dir: &str) -> Result<(), CatchpoleError> {
    // First we need to get a list of changed files.
    let changed_files = find_changed_files(root_dir)?;
    for file in changed_files {
        if !(file.ends_with(".cpp") || file.ends_with(".h")) {
            continue;
        }
        let repo_path = String::from(root_dir);
        let full_filename = if repo_path.ends_with('/') {
            format!("{}{}", repo_path, &file)
        } else {
            format!("{}/{}", repo_path, &file)
        };
        let parsed_comments = find_comments(&full_filename)?;

        lazy_static! {
            static ref RE: Regex =
                Regex::new(r"//\s*\[CATCHPOLE\]:(?P<comment>((\s*[A-Za-z][^.!?]*\.*)*))").unwrap();
        }
        let mut filtered_comments = parsed_comments
            .into_iter()
            .filter(|c| RE.is_match(&c.text))
            .collect::<Vec<ParsedComment>>();
        if !filtered_comments.is_empty() {
            for filtered_comment in &mut filtered_comments {
                let text = filtered_comment.text.clone();
                for cap in RE.captures_iter(&text) {
                    filtered_comment.update_text(cap[1].trim().to_string());
                }
            }
            return Ok(());
        }
    }

    Err(CatchpoleError::NoCommentsFound)
}


fn find_changed_files(path: &str) -> Result<Vec<String>, CatchpoleError>{
    let repo = git2::Repository::open(path)?;

    if repo.is_bare() {
        return Err(CatchpoleError::EmptyRepo);
    }

    let mut opts = git2::StatusOptions::new();
    opts.include_ignored(false);
    opts.include_untracked(false);
    opts.exclude_submodules(true);

    let statuses = repo.statuses(Some(&mut opts))?;
    let mut changed_files: Vec<String> = Vec::new();
    for status in statuses.iter() {
        // This line just isn't it folks
        changed_files.push(String::from(status.path().unwrap()))
    }
    Ok(changed_files)
}


#[must_use]
fn find_comments(filename: &str) -> Result<Vec<ParsedComment>, CatchpoleError> {
    // Acquire an instance of `Clang`
    let mut results: Vec<ParsedComment> = Vec::new();
    let clang = clang::Clang::new().unwrap();

    // Create a new `Index`
    let index = clang::Index::new(&clang, false, true);
    println!("Currently processing: {}", filename);
    // Parse a source file into a translation unit
    let tu = index.parser(filename).parse()?;
    let sr = tu.get_entity().get_range();
    let source_range = match sr {
        None => {
            return Err(CatchpoleError::NoSourceRange);
        }
        Some(_) => sr.unwrap(),
    };
    let tokens = source_range.tokenize();

    // Get the structs in this translation unit
    let comments = tokens
        .into_iter()
        .filter(|t| {
            return t.get_kind() == clang::token::TokenKind::Comment;
        })
        .collect::<Vec<_>>();

    for comment in comments {
        results.push(ParsedComment::new(
            comment.get_spelling(),
            String::from(
                comment
                    .get_location()
                    .get_file_location()
                    .file
                    .unwrap()
                    .get_path()
                    .to_str()
                    .unwrap(),
            ),
            comment.get_location().get_file_location().line,
        ))
    }

    Ok(results)
}
