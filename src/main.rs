use catchpole;
use std::process;

fn main() {
    if let Err(e) = catchpole::run("/home/tom/dev/projects/libstore") {
        println!("Application error: {}", e);
        process::exit(-1);
    }
}
