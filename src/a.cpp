//
// Created by tom on 19/08/2021.
//
struct A{
  int a
};
struct B : public A{
  char b;
};

void bar(){}

int foo(int, int){
  //  this is a comment
//  [CATCHPOLE]: This is a catchpole comment. With multiple sentences!
  auto a = 0;
  a += 1;
  bar();
  return a;
}